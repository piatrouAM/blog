from django.urls import path
 
from .views import get_list, get_post
 
urlpatterns = [
    path('', get_list, name='list'),
    path('<int:id>', get_post, name='detail')
]