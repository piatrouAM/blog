from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from .models import Post
from django.forms.models import model_to_dict
import logging
# Create your views here.

logger = logging.getLogger(__name__)

def get_list(req):
    logger.info(req.get_full_path(), extra={'ip': req.META.get('HTTP_X_FORWARDED_FOR')})
    return JsonResponse(list(Post.objects.values('id', 'title')), safe=False)

def get_post(req, id):
    logger.info(req.get_full_path(), extra={'ip': req.META.get('HTTP_X_FORWARDED_FOR')})
    try:
        return JsonResponse(model_to_dict(Post.objects.get(id=id)))
    except ObjectDoesNotExist:
        return(JsonResponse({'id': None}))